﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cosmos.System;
using Cosmos.System.FileSystem;
namespace TurtleCore.Filesystem
{
    public class TurtleFS
    {
        public static CosmosVFS fs = new CosmosVFS();
        public static List<Disk> disks;

        private static string _currentPath;
        private static string _currentVolume;

        public static bool Initialize(bool showInfo)
        {
            fs.Initialize(showInfo);

            disks = fs.GetDisks();

            return false;
        }

        public static void Format(int drive)
        {
            //untested
            disks[drive].FormatPartition(drive, "FAT32");
        }

        public static string GetCurrentPath()
        {
            return _currentPath;
        }

        public static string GetCurrentVolume()
        {
            return _currentVolume;
        }

        public static string GetFullPath()
        {
            return $@"{_currentVolume}:\{_currentPath}";
        }

        public static void SetPath(string currentPath)
        {
            if (Directory.Exists(GetFullPath()))
            {
                _currentPath = currentPath;
            }
            System.Console.WriteLine("Could not go to: " + currentPath);
        }


    }
}

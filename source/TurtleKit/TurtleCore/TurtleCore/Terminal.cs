﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleCore.TurtleCore.TurtleCore
{
    public class Terminal
    {
        private GraphicsBuffer _currentBuffer;

        private bool _isInitalized = false;

        public void Initialize(GraphicsBuffer gbuffer = GraphicsBuffer.NORMAL)
        {
            _currentBuffer = gbuffer;

            //Goes after init is done
            _isInitalized = true;
        }

        public bool IsInitialized
        {
            get
            {
                return _isInitalized;
            }
        }

        public GraphicsBuffer CurrentBuffer()
        {
            return _currentBuffer;
        }

        public enum GraphicsBuffer
        {
            NORMAL,
            VGA
        }
    }
}
